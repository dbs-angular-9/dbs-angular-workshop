var user = {
    firstName:"Santosh",
    lastName: "Kumar",
    age: 45,
    dept:"HR",
    isMarried: true,
    address:{
        city:"Bangalore",
        state: "Karnataka"
    },
    hobbies:["singing", "cricket"],

    //reffered as a method 
    fullName: function(){
        return this.firstName +" "+ this.lastName;
    }
}

console.log("Name "+ user.name);

console.log("City "+ user.address['city']);
console.log("Hobby "+ user.hobbies[0])

//augment the object

user.salary = 56000;
console.log("Salary "+user.salary)
console.log (user.fullName());