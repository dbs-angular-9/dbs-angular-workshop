

// functions assigned to variable

var add = function (a,b){return a + b}

var sub = function (a,b){return a - b}

var product = function(a,b){return a * b}

//functions as argument

var calc = function(a, b, fun){
    return fun(a,b);
}

var square = function(a){
    return a * a;
}

var result = calc(3,undefined, square);
console.log ("The square of number is "+ result);

//return a function from the method

var greet = function(name){
    return function(message){
        return name +" "+ message;
    }
}


var greetAniruddh = greet("Anirudh");

var result = greetAniruddh("welcome ");


//powerfull - closures

http://dev.dbs.app.com 
http://stage.dbs.app.com
http://prod.dbs.app.com

var urlFunc = function(scheme){
    return function(env){
        return scheme+"://"+env+"app.com"
    }
}

var env = urlFunc("https")
var devUrl = env("dev");
var devUrl = env("stage");



