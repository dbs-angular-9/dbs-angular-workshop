const indianTeam = ["Dhoni", "Kohli", "Raina", "Rohit","Rayudu"];


//array destructuring
const [captain, vCaptain, openingBatsman, ...rest] = indianTeam;

console.log(`The captain of Indian team is ${captain} followed by vice captain ${vCaptain} and opening batsman ${openingBatsman}`);

console.log(`The rest of the team members are ${rest}`)

//spread operator

const fruits = ["apple", "mango", "grapes", "oranges"]
const veggies = ["cucumber", "beetroot", "radish","carrot"]

const groceries = [...fruits, ...veggies];

console.log(groceries)

//use cases 

let batsman = "ganguly";
let runner = "sachin";



[batsman, runner] = [runner, batsman];

console.log(`After a single, the batsman is now ${batsman} and runner is now ${runner}`);

