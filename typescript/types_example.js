//implicit types
const age = 45;
const username = "Pradeep";
const isMarried = true;
//benefits of type system 
//explicit
const city = "Bangalore";
let state;
state = "Karnataka";
//arrow function 
const sum = (a, b) => a + b;
