# Steps to follow once you login to the desktop

- git config --global user.name <gitlab-username>
- git config --global user.email <gitlab-user email>
- Run the ssh-keygen.exe
- hit enter key to generate the id_rsa files
- Go to C:\Users\user-<id>\.ssh 
- Run the command - notepad id_rsa.pub
- Copy the content of the file
- Login to Gitlab 
- Under the settings, to to SSH-keys
- Add a new SSH-key
- Paste the key and close 

- Navigate to C: drive
- Run the clone command - git clone git@gitlab.com:dbs-angular-9/dbs-angular-workshop.git

Path for Typescript - C:\Users\user1\AppData\Roaming\npm\node_modules\typescript\bin
