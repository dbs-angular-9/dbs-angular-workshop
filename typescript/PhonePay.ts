import {Payment} from './Payment';

export class PhonePay implements Payment{
    pay(from:string, to:string, amount:number){
        console.log(`Amount ${amount} paid from ${from} to ${to} using Phone Pay`);
    }
}

export class GooglePay {
    pay(from:string, to:string, amount:number){
        console.log(`Amount ${amount} paid from ${from} to ${to} using Google Pay`);
    }
}