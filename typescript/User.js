"use strict";
exports.__esModule = true;
exports.User = void 0;
var User = /** @class */ (function () {
    function User(firstname, lastname, age, isMarried) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.isMarried = isMarried;
    }
    User.prototype.getFirstName = function () {
        return this.firstname;
    };
    User.prototype.setFirstName = function (firstname) {
        this.firstname = firstname;
    };
    return User;
}());
exports.User = User;
