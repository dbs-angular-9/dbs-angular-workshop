
export class User {
    constructor(private firstname: string, 
                private lastname:string, 
                private age?:number, 
                private married?: boolean){  }

    get firstName(){
        return this.firstname;
    }

    get lastName():string{
        return this.lastName;
    }

    get age(){
        return this.age;
    }

    get married(){
        return this.married;
    }   

}