
export interface Payment {

    pay(from:string, to: string, amount: number);
}