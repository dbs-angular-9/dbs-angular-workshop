import {Payment} from './Payment';
import { GooglePay, PhonePay } from './PhonePay';

const payment:Payment = new PhonePay();

payment.pay("Vinod", "Kashi", 45000);

const googlePay:Payment = new GooglePay();
googlePay.pay("Harish", "Krishna", 25000);